#! /usr/bin/env bash
set -e

git submodule update --init --recursive

MACHINE=`uname -m`_`uname -s`
CC=`which gcc`
CXX=`which g++`
export CC=$CC
export CXX=$CXX

mkdir -p build/$MACHINE
mkdir -p temp1/$MACHINE
mkdir -p temp2/$MACHINE

cd temp1/$MACHINE

../../sqlite/configure --disable-shared --prefix=`pwd`/../../build/$MACHINE/
make -j4
make install

cd ../../
cd temp2/$MACHINE

cmake ../../SQLiteCpp -DCMAKE_INSTALL_PREFIX=`pwd`/../../build/$MACHINE
make -j4
make install
